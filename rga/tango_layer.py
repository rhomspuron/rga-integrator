

from tango.server import class_property, device_property
import numpy as np
from tango import AttrQuality, AttrWriteType, DispLevel, DevState, DebugIt
from tango.server import Device, attribute, command, pipe, device_property
from .controller import RGAController


class MKSRGA(Device):

    host = device_property(dtype=str)
    port = device_property(dtype=int, default_value=10014)

    def init_device(self):
        Device.init_device(self)
        self.first_mass = 1
        self.set_state(DevState.STANDBY)
        self.rga = RGAController(self.host, self.port)

    # @attribute(label="IP")
    # def ip(self):
    #     return MyRGA.ip
    # @ip.write
    # def set_ip(self, ip):
    #     self.MyRga.set_ip(ip)

    @attribute(label="1st mass scan", unit="AMU", dtype=int, min_value=1,
               max_value=101)
    def first_mass(self):
        return self.rga.get_start_mass(self)

    @first_mass.write
    def set_first_mass(self, mass):
        self.first_mass = mass


if __name__ == "__main__":
    MKSRGA.run_server()

import datetime
import logging
from .file import File


def get_time():
    time = str(datetime.datetime.now())
    time = time.split(".")
    return time[0]


class PeakRecord(File):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                                ' %(levelname)s - %(message)s')

    def __init__(self):
        super().__init__()

        self.settings = []
        self.file_name = ""

    # def save_header(self):
    #     with open(self.file_name, "a") as file:
    #         for i in self.settings:
    #             file.write(str(i))
    #             file.write('\n')
    #         # file.write(str(self.settings))

    def save_header(self):
        with open(self.file_name, "a") as file:
            file.write('\n')
            n=0
            for i in self.settings:
                file.write('Mass' + '\t' + str(self.settings[n][0]) + '\n')
                file.write('Detector' + '\t' +
                           self.get_detector(self.settings[n][1]) + '\n')
                file.write('Accuracy'+ '\t' + str(self.settings[n][2])+ '\n')
                file.write('Gain' + '\t' +
                           self.get_gain(self.settings[n][3])+ '\n')
                file.write('\n')
                n += 1

    def save_line(self, line):
        line = line.split(' ')
        line[1]=float(line[1])
        line[2]=float(line[2])
        line[1] = '%.2f' % line[1]
        line[2] = float("{:.2e}".format(line[2]))
        if line[2] < 0:
            line[2] = 0

        with open(self.file_name, "a") as file:

            file.write(get_time())
            file.write('\t')
            file.write(line[1])
            file.write('\t')
            file.write(str(line[2]))
            file.write('\n')
import datetime
import logging


def get_time():
    time = str(datetime.datetime.now())
    time = time.split(".")
    return time[0]

class File:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self):

        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)

    def create_file(self, filename):

        time = str(datetime.datetime.now())
        time = time.split(".")
        fl = str(filename), str(time[0])
        self.file_name = "".join(fl)
        self.log.info('File {a} created'.format(a=self.file_name))

        with open(self.file_name, "w+") as file:
            file.write(self.file_name + "\n")

        self.save_header()

    def save_header(self):
        pass


    def save_analog_line(self, line):
        with open(self.file_name, "a") as file:

            file.write("\n")
            file.write(get_time())
            file.write("\n")
            for i in line:
                file.write(i)

            # for i in line:
            #     i = i.split(" ")
            #     i[1] = float(i[1].replace("\r\n\r\r\r", ""))
            #     i[1] = '%.2f' % i[1]
            #
            #     i[2] = float(i[2].replace("\r\n\r\r\r", ""))
            #     i[2] = float("{:.2e}".format(i[2]))
            #     # print (float(i[2]))
            #     if i[2] < 0:
            #         i[2] = 0
            #     print(i[1], i[2])
            #     str_to_store = (str(i[1]) + "\t" + str(i[2]))
            #     file.write(str_to_store)
            #     file.write("\n")
        self.log.info('Line attached')




    def get_gain(self, index):
        gains = ['1', '100', '2000']
        return gains[index]

    def get_detector(self, index):
        detectors = ['Faraday', 'Channeltron -650v', 'Channeltron -750v',
                     'Channeltron -900v']
        return detectors[index]

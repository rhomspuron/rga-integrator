import telnetlib
import logging
import time


class RgaTelnet:
    code = 'ascii'
    end_line = "\r\r".encode(code)
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, host, port=10014, timeout=3):

        log_name = '{}RgaTelnet'.format(__name__)
        self.log = logging.getLogger(log_name)

        host = host.encode(self.code)
        self.tl = telnetlib.Telnet(host, port, timeout)
        self.tl.read_until(self.end_line, 1)
        self.tl.write("Control telnet 1 \n".encode(self.code))
        print(self.read())
        time.sleep(1)
        self.log.info('Connect to %s:%s', host, port)

    def read(self):
        r = self.tl.read_until(self.end_line, 1)
        r = r.decode(self.code)
        self.log.info('Returned Message: {a}'.format(a=r))
        return r

    def send_cmd(self, cmd):
        self.log.info('Command Send: {a} '.format(a=cmd))
        cmd = (cmd + "\n")

        cmd_enc = cmd.encode(self.code)
        self.tl.write(cmd_enc)
        r = self.read()
        self.validate(cmd, r)

        return r

    def validate(self, cmd, ret):

        ret = ret.split('\n')
        ret = ret[0]
        c = cmd.split()
        expected = c[0] + " OK"
        # print('RET=', ret)
        # print('C=', expected)

        if ret != expected:
            self.log.error('Returned Statement not expected: {a}. Expected {b}'
                           .format(a=ret, b=expected))

    def send_no_val(self, cmd):
        self.log.info('Command Send: {a} '.format(a=cmd))
        self.tl.write(cmd.encode(self.code))
        r = self.read()
        return r

    def read_analog(self):
        #  while True:
        #      r = self.read()
        #
        # # if r.find("Mass") > -1:
        # #     print("R:", r)
        #      return r

        r = self.tl.read_until(self.end_line, 3)
        r = r.decode(self.code)
        return r


if __name__ == "__main__":
    rga = RgaTelnet("192.168.1.166")
    # print(rga.read())
    # print(rga.read())
    # time.sleep(1)
    print(rga.send_cmd("DetectorInfo 0"))

    # print(rga.read())
    # rga.send_cmd("AddAnalog Analog1 1 10 4 5 0 0 0\r")
    # print(rga.read())
    # rga.send_cmd("ScanAdd Analog1\r")
    # rga.send_cmd("ScanStart 1\r")
    # while True:
    #     print(rga.read())

import logging
import threading
from .peak_record import PeakRecord


class PeakScan:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, send):

        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)
        self.send = send

        self.scan_stop = threading.Event()
        # [[mass, detector, accuracy,gain]]
        self.scan_settings = []

        self.record = PeakRecord()
        self.recording = False

    def send_cmd(self, cmd):
        self.send.__send_cmd(cmd)

    def start_peak_scan(self):
        peak_tr = threading.Thread(target=self.peak_loop)
        self.scan_stop = threading.Event()
        peak_tr.start()
        # time.sleep(10)
        # self.scan_stop.set()

    def peak_loop(self):

        if len(self.scan_settings) < 1:
            self.log.info('No masses added to the scan')
        else:
            self.log.info('Init Peak Scan Loop')

            # str_to_send=AddSinglePeak Name Mass Accuracy Gain Source Detector
            # str_to_send=AddSinglePeak  SinglePeak scan_settings
            # [[mass, detector, accuracy,gain]]
            n = 0
            while not self.scan_stop.isSet():

                for i in self.scan_settings:
                    str_to_send = ("AddSinglePeak " + 'SinglePeak' + ' ' + str(
                        self.scan_settings[n][0]) + ' ' + str(
                        self.scan_settings[n][2]) + ' ' +
                                   str(self.scan_settings[n][3]) + ' ' + str(0)
                                   + ' ' + str(self.scan_settings[n][1]) + str(
                                '\n'))
                    print(str_to_send)
                    self.send.send_cmd(str_to_send)
                    self.send.send_cmd('ScanAdd SinglePeak')
                    self.send.send_cmd("ScanStart 1")

                    r = self.send.read()
                    while not r.find("MassReading") > -1:
                        r = self.send.read()
                    print(r)
                    if self.recording:
                        self.record.save_line(r)
                    # if r.find("MassReading") > -1:
                    #     return r

                    self.send.send_cmd('ScanStop')
                    self.send.send_cmd('MeasurementRemoveAll')

                    n += 1

                n = 0

            # self.scanning = False
    def start_peak_record(self,file_name):

        self.record.settings=self.scan_settings
        self.record.create_file(file_name)
        self.recording = True

    def save_peak_line(self, line):
        self.record.save_line(line)

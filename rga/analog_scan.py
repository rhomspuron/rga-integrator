import logging
import threading
from attributes import Attributes
from record_scan import AnalogRecord


class AnalogScan:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, send):

        log_name = '{}Record'.format(__name__)

        self.log = logging.getLogger(log_name)

        self.scan_stop = threading.Event()
        self.send = send

        self.start_mass = 1
        self.end_mass = 6
        self.points = 4
        self.accuracy = 2
        self.gain = 0
        self.source = 0
        self.detector = 0

        self.analog_settings = [
            self.start_mass,
            self.end_mass,
            self.points,
            self.accuracy,
            self.gain,
            self.source,
            self.detector
        ]
        self.scanning = False
        self.record = AnalogRecord()
        self.bunch_scan = []
        self.recording = False

        # self.analog_tr = threading.Thread(target=self.analog_loop, daemon=True)
        # self.scan_stop = threading.Event()
        self.attributes = Attributes(self.start_mass, self.end_mass)

    def send_cmd(self, cmd):
        self.send.send_cmd(cmd)

    def start_analog(self):
        str_to_send = 'AddAnalog analog1 '
        for i in self.analog_settings:
            i = str(i)
            str_to_send += i + ' '

        print(str_to_send)
        self.send_cmd(str_to_send + '')
        self.send_cmd('ScanAdd analog1')
        self.send_cmd('ScanStart 1')
        analog_tr = threading.Thread(target=self.analog_loop)
        analog_tr.start()

    def analog_loop(self):

        n_scan = 0
        while not self.scan_stop.isSet():
            self.scanning = True
            r = self.send.read_analog()

            if r.find("Mass") > -1:

                r = r.split()
                r[1] = '%.2f' % float(r[1])
                r[2] = float(r[2])
                r[2] = float("{:.2e}".format(r[2]))

                self.line = (str(r[1]) + "\t" + str(r[2]))
                self.bunch_scan.append(self.line + "\n")
                print(self.line)

                if float(r[1]) % 1 == 0:
                    self.attributes.mass_attr[float(r[1])] = r[2]
            if not r:
                self.log.info('Scan Done')
                print("BUNCH:", self.bunch_scan)
                print('n_scan= ', n_scan)
                print(self.attributes.mass_attr)
                if self.recording:
                    self.save_analog_line(self.bunch_scan)
                self.bunch_scan = []
                self.send.send_cmd('ScanStop')
                self.send.send_cmd('ScanAdd analog1')
                self.send.send_cmd('ScanStart 1')
                n_scan = +1

        self.scanning = False

    """analog record loop"""

    def start_analog_record(self, file_name):
        self.record.settings = self.analog_settings
        self.record.create_file(file_name)
        self.recording = True
        # self.my_record.create_analog_header(self.analog_settings)
        # self.my_record.save_analog_header()

    def save_analog_line(self, line):
        self.record.save_analog_line(line)

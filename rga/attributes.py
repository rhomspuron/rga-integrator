import logging


class Attributes:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self, first_mass, last_mass):

        self.first_mass = first_mass
        self.last_mass = last_mass
        self.mass_value = 0

        # self.mass_attr = {}
        self.scan_result = []
        self.mass_list = []

        def do_masses_list():
            for i in range(1, 101):
                self.mass_list.append(int(i))

        def do_scan_results():
            for i in range(1, 101):
                self.scan_result.append(0)

            print(self.mass_list)
            print(self.scan_result)

        do_masses_list()
        do_scan_results()
        self.mass_attr = dict(zip(self.mass_list, self.scan_result))
#
# if __name__ == "__main__":
#
#     r = Attributes()


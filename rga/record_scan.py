import datetime
import logging
from file import File


# def get_time():
#     time = str(datetime.datetime.now())
#     time = time.split(".")
#     return time[0]


class AnalogRecord(File):
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')

    def __init__(self):

        super().__init__()
        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)

        # self.myFile
        # self.MyFile = self.create_file()
        # self.name = ""
        # time= str(datetime.datetime.now())
        # time=time.split(".")
        # self.file_name=(FileName + time[0])
        # self.myFile = open(self.file_name, "w+")
        self.settings = []
        self.file_name = ""
        # self.save_header()

    # def create_file(self, filename):
    #
    #     time = str(datetime.datetime.now())
    #     time = time.split(".")
    #     fl = str(filename), str(time[0])
    #     self.file_name = "".join(fl)
    #     self.log.info('File {a} created'.format(a=self.file_name))
    #     # self.MyFile = open(self.name, "w+")
    #
    #     with open(self.file_name, "w+") as file:
    #         file.write(self.file_name + "\n")
    #
    #     self.save_analog_header(self.settings)

        # return self.MyFile

    def save_header(self):

        naming = ["start mass= ", "end mass= ", "points/mass= ",
                  "accuracy= ", "gain= ", "detector = "]

        self.settings[4] = self.get_gain(self.settings[4])
        self.settings[5] = self.get_detector(self.settings[5])
        list_settings = tuple(zip(naming, self.settings))

        with open(self.file_name, "a") as file:
            for i in list_settings:

                i = str(i)
                i=(i[2:-1])
                # i = i.replace("(", "")
                # i = i.replace(")", "")
                i = i.replace("'", "")
                i = i.replace(",", "\t")
                print(i)
                file.write(i + "\n")
            self.log.info("attached header: {a}".format(a=list_settings))

    def create_analog_header(self, settings):
        naming = ["start mass= ", "end mass= ", "points/mass= ",
                  "accuracy= ", "gain= ", "detector = "]

        settings[4] = self.get_gain(settings[4])
        settings[5] = self.get_detector(settings[5])
        list_settings = tuple(zip(naming, settings))
        return list_settings

    def get_gain(self, index):
        gains = ['1', '100', '2000']
        return gains[index]

    def get_detector(self, index):
        detectors = ['Faraday', 'Channeltron -650v', 'Channeltron -750v',
                     'Channeltron -900v']
        return detectors[index]

    # def save_line(self, line):
    #     with open(self.file_name, "a") as file:
    #
    #         file.write("\n")
    #         file.write(get_time())
    #         file.write("\n")
    #
    #         for i in line:
    #             i = i.split(" ")
    #             i[1] = float(i[1].replace("\r\n\r\r\r", ""))
    #             i[1] = '%.2f' % i[1]
    #
    #             i[2] = float(i[2].replace("\r\n\r\r\r", ""))
    #             i[2] = float("{:.2e}".format(i[2]))
    #             # print (float(i[2]))
    #             if i[2] < 0:
    #                 i[2] = 0
    #             print(i[1], i[2])
    #             str_to_store = (str(i[1]) + "\t" + str(i[2]))
    #             print ('Str: =', str_to_store)
    #             file.write(str_to_store)
    #             file.write("\n")
    #     self.log.info('Line attached')

if __name__ == '__main__':

    f = AnalogRecord()

    start_mass = 1
    end_mass = 6
    points = 4
    accuracy = 2
    gain = 0
    source = 0
    detector = 0

    b = [start_mass, end_mass, points, accuracy, gain, source, detector]
    print(b)
    f.settings = b
    f.create_file("/home/lluis/Escritorio/RGAs/testFile/testFile")

    # f.save_header()
    f.save_analog_line("sfkehsdkh")


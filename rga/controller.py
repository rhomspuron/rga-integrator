
import numpy as np
import threading
import time
import logging

from .comm import RgaTelnet
from .analog_scan import AnalogScan
from .peak_scan import PeakScan


class RGAController:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(threadName)s - '
                               ' %(levelname)s - %(message)s')


    def __init__(self, ip, port=10014):
        log_name = '{}Record'.format(__name__)
        self.log = logging.getLogger(log_name)

        self.ip = ip
        self.send = RgaTelnet(ip, port)
        self.analog_scan = AnalogScan(self.send)
        self.peak_scan =PeakScan(self.send)

        self.state = ['Running', 'On', 'StandBy', 'fault']

        self.amount_detectors=0
        """default degass values"""

        self.start_power = 30
        self.end_power = 80
        self.ramp = 90
        self.dwell = 240
        self.reset_time = 30

        self.degas_settings = [
            self.start_power,
            self.end_power,
            self.ramp,
            self.dwell,
            self.reset_time
        ]

        self.__n_detectors()
    # General Settings

    def __send_cmd(self, cmd):
        r = self.send.send_cmd(cmd)
        return r

    def __n_detectors(self):
        r = self.__send_cmd('DetectorInfo 0')
        r = r.split("\n")
        self.amount_detectors = len(r[3:-2])

    def ion_energy(self, value):
        if value not in np.arange(0, 11):
            print("Must be between 0 and 10 eV")
        else:
            str_to_send = ('SourceIonEnergy 0 ' + str(value))
            # self.my_rga.ion_energy(value)
            print(self.__send_cmd(str_to_send))

    def emission(self, value):
        if value not in np.arange(0, 6):
            print('Emission Current must be between 0 and 5 mA')
        else:
            # self.my_rga.emission(value)
            str_to_send = ('SourceEmission 0' + str(value))
            print(self.__send_cmd(str_to_send))

    # def electron_energy(self, value):
    #     str_to_send = ('SourceElectronEnergy 0 ' + str(value))
    #     print(self.send.send_cmd(str_to_send))

    def electron_energy(self, value):
        if value not in np.arange(0, 101):
            print('Electron Energy must be between 0 and 100 eV')
        else:
            # self.my_rga.electron_energy(value)
            str_to_send = ('SourceElectronEnergy 0 ' + str(value))
            print(self.__send_cmd(str_to_send))

    # def extraction(self, value):
    #     str_to_send = ('SourceExtract 0 ' + str(value))
    #     print(self.send.send_cmd(str_to_send))

    def extraction(self, value):
        if not -130 <= value <= 1:
            raise ValueError('Extraction Voltage must be between -130 and 0 V')

        # self.my_rga.extraction(value)
        str_to_send = ('SourceExtract 0 ' + str(value))
        print(self.__send_cmd(str_to_send))

    # def align_low(self, value):
    #     s = self.send_cmd("SourceLowMassAlignment 0 " + str(value))
    #     if s == "\r\rSourceLowMassAlignment OK\r":
    #         print("New LOW mass alignment value: ", value)
    #     else:
    #         print("Something went wrong..")

    def align_low(self, value):
        if value not in np.arrange(1, 65535):
            print("Align value must be < 65535")
        else:
            str_to_send=("SourceLowMassAlignment 0 " + str(value))
            print(self.__send_cmd(str_to_send))
            # self.my_rga.align_low(value)

    # def align_high(self, value):
    #     s = self.send.send_cmd("SourceHighMassAlignment 0 " + str(value))
    #     if s == "rSourceHighMassAlignment OK":
    #         print("New HIGH mass alignment value: ", value)
    #     else:
    #         print("Something went wrong..")

    def align_high(self, value):
        if value not in np.arrange(1, 65535):
            print("Align value must be < 65535")
        else:
            str_to_send= "SourceHighMassAlignment 0 " + str(value)
            # self.my_rga.align_high(value)
            self.__send_cmd(str_to_send)

    # def resolution_low(self, value):
    #     str_to_send = ('SourceLowMassResolution 0 ' + str(value))
    #     self.send.send_cmd(str_to_send)

    def resolution_low(self, value):
        if value not in np.arrange(1, 65535):
            print("Resolution value must be < 65535")
        else:
            str_to_send = ('SourceLowMassResolution 0 ' + str(value))
            self.__send_cmd(str_to_send)

    # def resolution_high(self, value):
    #     str_to_send = ('SourceHighMassResolution 0 ' + str(value))
    #     self.send.send_cmd(str_to_send)

    def resolution_high(self, value):
        if value not in np.arrange(1, 65535):
            print("Resolution value must be < 65535")
        else:
            str_to_send = ('SourceHighMassResolution 0 ' + str(value))
            self.send.send_cmd(str_to_send)

    def diagnostic(self):
        self.__send_cmd('RunDiagnostics')
        # self.send.read()
        # print(self.send.read())
        return self.send.read()
    # analog scan settings


    # to check

    # def get_state(self):
    #     if self.scanning:
    #         return self.state[0]
    #     elif self.filamentInfo() == "off":
    #         print("filON")

    def filament_info(self):
        a = self.send.send_cmd("FilamentInfo")
        print(a)

    """filament management"""

    def select_filament(self, value):
        str_to_send = ('FilamentSelect ' + str(value))
        self.send.send_cmd(str_to_send)

    def filament_state(self, value):
        str_to_send = ('FilamentControl ' + value)
        self.__send_cmd(str_to_send)

    def degas_start_power(self, value):
        self.start_power = value

    def degas_end_power(self, value):
        self.end_power = value

    def degas_ramp(self, value):
        self.ramp = value

    def degas_dwell(self, value):
        self.dwell = value

    def degas_reset(self, value):
        self.reset_time = value

    def degas_start(self):
        if self.start_power <= self.end_power:
            str_to_send = 'StartDegas '
            for i in self.degas_settings:
                i = str(i)
                str_to_send += i + ' '
            self.send.send_cmd(str_to_send)
        else:
            print("End power must be higher than init power")

    # def degas_stop(self):
    #     #todo


    def remove_measures(self):
        self.send.send_cmd('MeasurementRemoveAll\r')

    # analog scan

    def set_start_mass(self, value):
        if value not in (np.arange(0, 101)):
            print("1st Mass must be between 1 and 100")
        else:
            self.analog_scan.analog_settings[0] = value

    def set_end_mas(self, value):
        self.analog_scan.analog_settings[1] = value

    def set_points_per_mass(self, value):
        self.analog_scan.analog_settings[2] = value

    def set_accuracy(self, value):
        self.analog_scan.analog_settings[3] = value

    def set_gain(self, value):
        if value not in (1, 100, 2000):
            print("gain index must be 1 or 100 or 20000")
        else:
            self.analog_scan.analog_settings[4] = value

    def set_detector(self, value):
        if value not in np.arange(0, self.amount_detectors):
            print("detector index must be between 0 and {a}".
                  format(a=self.amount_detectors))
        else:
            self.analog_scan.analog_settings[6] = value

    def start_analog_scan(self):
        self.analog_scan.start_analog()
        # str_to_send = 'AddAnalog analog1 '
        # for i in self.analog_settings:
        #     i = str(i)
        #     str_to_send += i + ' '
        #
        # print(str_to_send)
        # self.send.send_cmd(str_to_send + '')
        # self.send.send_cmd('ScanAdd analog1')
        # self.send.send_cmd('ScanStart 1')
        #
        # self.analog_tr.start()

        """check stop scan"""
        # time.sleep(2)
        # print("RECORD==", self.recording)
        # # self.recording = True
        # time.sleep(2)
        # print("RECORD==", self.recording)
        # time.sleep(2)
        # print("RECORD==", self.recording)
        # print(self.get_state())
        # time.sleep(2)
        # self.scan_stop.set()

        """end checking stop scan"""

    # def analog_loop(self):
    #     while not self.scan_stop.isSet():
    #         print(self.recording)
    #         self.scanning = True
    #         r = self.send.read_analog()
    #
    #         if r.find("Mass") > -1:
    #             self.bunch_scan.append(r + "\n")
    #             print(r)
    #         if not r:
    #             print("BUNCH:", self.bunch_scan)
    #             if self.recording:
    #                 self.save_analog_line(self.bunch_scan)
    #             self.bunch_scan = []
    #             self.send.send_cmd('ScanStop')
    #             self.send.send_cmd('ScanAdd analog1')
    #             self.send.send_cmd('ScanStart 1')
    #
    #             # r = self.send.read()

        # self.scanning = False

    """analog record loop"""
    def start_analog_record(self,filename):
        self.analog_scan.start_analog_record(filename)

    def record_stop(self):
        self.analog_scan.recording = False
        self.peak_scan.recording = False

    def scan_stop(self):
        self.analog_scan.scan_stop.set()
        self.peak_scan.scan_stop.set()

    # def start_analog_record(self, fileName):
    #
    #     print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx")
    #     self.record.settings = self.analog_settings
    #     self.record.create_file(fileName)
    #     self.recording = True
    #     # self.my_record.create_analog_header(self.analog_settings)
    #     # self.my_record.save_analog_header()
    #
    # def save_analog_line(self, line):
    #     self.record.save_line(line)

    """peak jump mode"""
    def peak_scan_add_mass(self, mass):
        if len(mass) != 4:
            print("Structure of add mass: mass, detector, accuracy, gain" )
        else:
            self.peak_scan.scan_settings.append(mass)
            self.log.info('Added to scan {a}'.format(a=mass))

    def start_peak_scan(self):
        self.peak_scan.start_peak_scan()

    # def get_peak_masses(self, masses, value):
    #     peak_tr = threading.Thread(target=self.peak_loop, args=(masses, value))
    #     self.scan_stop = threading.Event()
    #     peak_tr.start()
    #     time.sleep(10)
    #     self.scan_stop.set()
    #
    # def peak_loop(self, masses, value):
    #     """ str_to_send=AddSinglePeak Name Mass Accuracy Gain Source Detector"""
    #     """str_to_send=AddSinglePeak  SinglePeak masses[0]  value masses[2] 0 masses[1]"""
    #     n = 0
    #     while not self.scan_stop.isSet():
    #         for i in masses:
    #             str_to_send = ("AddSinglePeak " + 'SinglePeak' + ' ' + str(
    #                 masses[n][0])
    #                            + ' ' + str(value) + ' ' + str(
    #                         masses[n][2]) + ' ' + str(0)
    #                            + ' ' + str(masses[n][1]) + str('\n'))
    #
    #             self.send.send_cmd(str_to_send)
    #
    #             r = (self.do_peak_scan(str_to_send))
    #             self.send.send_cmd('ScanStop')
    #             self.send.send_cmd('MeasurementRemoveAll')
    #             time.sleep(0.25)
    #             n += 1
    #             return r
    #         n = 0
    #
    #     self.scanning = False
    """THIS WORKS
        # str_to_send = ("AddSinglePeak " + 'SinglePeak' + ' ' + str(masses[n][0])
        #            + ' ' + str(value) + ' ' + str(masses[n][2]) + ' ' + str(0)
        #            + ' ' + str(masses[n][1]) + str( '\n'))
        """

    # def do_peak_scan(self, str_to_send):
    #
    #     self.send.send_cmd(str_to_send)
    #     self.send.send_cmd('ScanAdd SinglePeak')
    #     self.send.send_cmd("ScanStart 1")
    #     r = self.send.read_peak()
    #     if r.find("MassReading") > -1:
    #         return r
    #
    #     # print(self.send.send_cmd("ScanStop"))
    #
    # def peak_add_mass(self, settings):
    #     if settings[0] > 100:
    #         print ('Max mass value mut be <100')
    #     elif settings[]:
    #     self.peak_scan.scan_settings.append(settings)


    def start_peak_record(self,filename):
        self.peak_scan.start_peak_record(filename)


if __name__ == "__main__":
    r=RGAController('192.168.1.166')
    r.set_gain(0)
    r.set_start_mass(4)
    r.set_detector(2)
    r.start_analog()
    time.sleep(2)
    r.start_analog_record('/home/lluis/Escritorio/RGAs/testFile/')
